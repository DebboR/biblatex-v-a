# BibLatex-V&A
Deze repository bevat een BibLatex stijl die de regels van [Wolter Kluwer's Juridische Verwijzingen en Afkortingen](https://legalworld.wolterskluwer.be/media/8828/v_a_juridische-verwijzingen-afkortingen.pdf) implementeert.

Deze stijl is gebaseerd op de Duitse stijl, geimplementeerd in de stijl `biblatex-jura`.

## Installatie
Installeer deze stijl door de bestanden `biblatex-va.bbx` en `biblatex-va.cbx` te kopieren naar `<TEXMFLOCAL>/tex/latex/biblatex-contrib/biblatex-va/` waar `<TEXMFLOCAL>` de folder van de lokale TeX-installatie is.

## Gebruik
Om deze stijl te gebruiken met BibLatex, gebruik je volgend LaTeX-commando: 
`\usepackage[style=biblatex-va]{biblatex}`

## Geimplemendeerde regels
TODO