#!/bin/bash -e
# This script copies the files to the TeX root folder.

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
ROOT_FOLDER=/mnt/c/Programs/MiKTeX\ 2.9/tex/latex/biblatex
BBX_FOLDER=$ROOT_FOLDER/bbx
CBX_FOLDER=$ROOT_FOLDER/cbx
BASE_NAME=biblatex-va

cp "$DIR/../$BASE_NAME.bbx" "$BBX_FOLDER"
cp "$DIR/../$BASE_NAME.cbx" "$CBX_FOLDER"
echo "Done copying files!"
